from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateForm, CreateItemForm

# Create your views here.
def todo_list(request):
    todo = TodoList.objects.all()
    context = {"todo_list": todo}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_detail": todos,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=post)
        if form.is_valid():
            lists = form.save()
            return redirect("todo_list_detail", id=lists.id)
    else:
        form = CreateForm(instance=post)
    context = {"post_object": post, "post_form": form}
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todolist_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = CreateItemForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = CreateItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)
